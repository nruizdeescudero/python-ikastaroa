bez = 0
totala = 0
saskia = []
prezioa = input('Sartu erosgaia eta prezioa (edo 0 bukatzeko):')

while prezioa != '0':
    erosgaia = prezioa.split(',')
    uneko_prezioa = int(erosgaia[1])
    uneko_erosgaia = erosgaia[0]
    bez += uneko_prezioa * 0.21
    totala += uneko_prezioa
    saskia.append(erosgaia)
    prezioa = input('Erosgai gehiago? Sartu erosgaia eta prezioa: ')


print(40*'-')
print('{0}{1}TIKETA{1}{0}'.format(2*'-',15*' '))
print(40*'-')
for erositakoa in saskia:
    print('\t{erosgaia}: {prezioa}'.format(erosgaia=erositakoa[0],\
                                           prezioa=erositakoa[1]))

print(40*'-')
print('BEZ aurretik:\t{}'.format(totala))
print('BEZ:\t{}'.format(bez))
print('GUZTIRA:\t{:.2f}'.format(totala+bez))

